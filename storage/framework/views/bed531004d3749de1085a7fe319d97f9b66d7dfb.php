<?php
  use App\Setting;
?>
<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b></b> 
  </div>
  <strong>Copyright &copy; <?php echo e(date('Y')); ?> <?php echo e(Setting::find(1)->site_name); ?>

</footer>
