<?php
use App\Role;
?>

<?php $__env->startSection('poststyles'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<?php if(Auth::user()->role_id==1): ?>
  <?php echo $__env->make('dashboards.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>;
<?php elseif(Auth::user()->role_id==2): ?>
  <?php echo $__env->make('dashboards.student', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>;
<?php elseif(Auth::user()->role_id==3): ?>
  <?php echo $__env->make('dashboards.teacher',['courses'=>$courses], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>;
<?php endif; ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('postscripts'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>