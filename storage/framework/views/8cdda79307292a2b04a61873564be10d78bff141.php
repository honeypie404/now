<?php $__env->startSection('poststyles'); ?>
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo e(asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="col-md-6">
  <div class="box box-primary">
    <div class="box-header  with-border">
      <h3 class="box-title">Assignments</h3>
      
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>Title</th>
          <th>CNo.</th>
          <th>Description</th>
          <th>Edit</th>
          <th>Delete</th>
        </tr>
        </thead>
        <tbody>
          <?php $__currentLoopData = $assignments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $assignment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
              <td><?php echo e($assignment->title); ?></td>
              <td><?php echo e($assignment->cno); ?></td>            
              <td><?php echo e($assignment->description); ?></td>
              <td>
              <a href="<?php echo e(route('assignments.edit',['id'=>$assignment->id])); ?>" class="btn btn-xs btn-info">
                Edit</span>
                </a>
                </td>
              <td>
                <a href="<?php echo e(route('assignments.delete',['id'=>$assignment->id])); ?>" class="btn btn-xs btn-danger">
                                  <span class="glyphicon glyphicon-trash"></span>
                </a>
              </td>
              
            </tr>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
</div>
  <div class="col-md-6">
  <?php echo $__env->make('admin.includes.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">Create assignment</h3>
      
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <form class="form-horizontal" method="POST" action="<?php echo e(route('assignments.store')); ?>">
        <?php echo e(csrf_field()); ?>

        <div class="form-group">
          <label for="title" class="col-sm-2 control-label">Title</label>

          <div class="col-sm-10">
            <input type="text" class="form-control" id="title" name="title" placeholder="Course Name">
          </div>
        </div>


        <div class="form-group">
          <label for="course" class="col-sm-2 control-label">CNo.</label>
       
          <div class="col-sm-10">
          <select class="form-control select2" multiple="multiple" id="course[]" name="course[]" data-placeholder="Select courses"
                  style="width: 100%;">
                  <?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $course): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($course->id); ?>"><?php echo e($course->title); ?> - <?php echo e($course->cno); ?></option>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </select>
        </div>
        </div>
        
     
        <div class="form-group">
          <label for="description" class="col-sm-2 control-label">Description</label>

          <div class="col-sm-10">
            <textarea class="form-control" id="description" name="description" placeholder="Description"></textarea>
          </div>
        </div>
        <div class="box-footer">
          
          <button type="submit" class="btn btn-success pull-right">Save</button>
        </div>
      </form>
    </div>
    <!-- /.box-body -->
  </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('postscripts'); ?>
  <!-- DataTables -->
  <script src="<?php echo e(asset('bower_components/datatables.net/js/jquery.dataTables.min.js')); ?>"></script>
  <script src="<?php echo e(asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')); ?>"></script>
  <script>
    $(function () {
      // $('#example1').DataTable()
      $('#example1').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
      })
    })
  </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>