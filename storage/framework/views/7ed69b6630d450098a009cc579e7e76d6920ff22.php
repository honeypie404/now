<?php $__env->startSection('poststyles'); ?>
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo e(asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('bower_components/select2/dist/css/select2.min.css')); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="col-md-6">
  <div class="box box-primary">
    <div class="box-header  with-border">
      <h3 class="box-title">Assigned Courses</h3>
      
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>Teacher</th>
          <th>Course</th>
          <th>Delete</th>
        </tr>
        </thead>
        <tbody>
          <?php $__currentLoopData = $teachers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $teacher): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
              <td><?php echo e($teacher->name); ?></td>
              <td>
                <?php $__currentLoopData = $teacher->courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $course): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <button type="button" class="btn btn-block btn-info"><?php echo e($course->title); ?></button>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php if(count($teacher->courses)==0): ?>
                    No assigned courses.
                <?php endif; ?>
              </td>
              <td>
                <?php if(count($teacher->courses)!=0): ?>
                  <a href="<?php echo e(route('assignment.delete',['id'=>$teacher->id])); ?>" class="btn btn-xs btn-danger">
                                    <span class="glyphicon glyphicon-trash"></span>
                  </a>
                <?php endif; ?>
              </td>
            </tr>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
</div>
  <div class="col-md-6">
  <?php echo $__env->make('admin.includes.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">Assign Course</h3>
      
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <form class="form-horizontal" method="POST" action="<?php echo e(route('course.assign')); ?>">
        <?php echo e(csrf_field()); ?>

        <div class="form-group">
          <label for="rolename" class="col-sm-2 control-label">Teacher</label>

          <div class="col-sm-10">
            <select name="teacher" class="form-control" id="teacher">
              <?php $__currentLoopData = $teachers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $teacher): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option value="<?php echo e($teacher->id); ?>"><?php echo e($teacher->name); ?></option>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="course[]" class="col-sm-2 control-label">Courses</label>

          <div class="col-sm-10">
            <select class="form-control select2" multiple="multiple" id="course[]" name="course[]" data-placeholder="Select courses"
                    style="width: 100%;">
                    <?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $course): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <option value="<?php echo e($course->id); ?>"><?php echo e($course->title); ?> - <?php echo e($course->cno); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
          </div>
        </div>
        <div class="box-footer">
          
          <button type="submit" class="btn btn-success pull-right">Save</button>
        </div>
      </form>
    </div>
    <!-- /.box-body -->
  </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('postscripts'); ?>
  <!-- DataTables -->
  <script src="<?php echo e(asset('bower_components/datatables.net/js/jquery.dataTables.min.js')); ?>"></script>
  <script src="<?php echo e(asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')); ?>"></script>
  <script src="<?php echo e(asset('bower_components/select2/dist/js/select2.full.min.js')); ?>"></script>
  <script>
    $(function () {
      $('.select2').select2()
      // $('#example1').DataTable()
      $('#example1').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
      })
    })
  </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>