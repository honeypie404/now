<?php $__env->startSection('poststyles'); ?>
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo e(asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')); ?>">
  <link rel="stylesheet" href="<?php echo e(asset('bower_components/select2/dist/css/select2.min.css')); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
  <div class="col-md-6">
  <div class="box box-primary">
    <div class="box-header  with-border">
      <div class="alert alert-success" role="alert">
        Class of <b><?php echo e($season->title); ?></b> on <?php echo e($course->title); ?> | Student sheet.
      </div>
      
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>Student</th>
          <th>Answers</th>
          <th>Evaluation</th>
          <th>Delete</th>
        </tr>
        </thead>
        <tbody>
          <?php $__currentLoopData = $season->users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $student): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
              <td><?php echo e($student->name); ?></td>

              <td>
                <a href="<?php echo e(route('answers')); ?>" class="btn btn-xs btn-info">
                                  Answers</span>
                </a>
               
              </td>
              <td>
              <a href="<?php echo e(route('evaluation',['id'=>$season->id,'course'=>$course->id])); ?>"class="btn btn-xs btn-info">
                                  Evaluation</span>
                </a>
                </td>
              <td>
                <a href="<?php echo e(route('class.assignment.delete',['season'=>$season->id,'student'=>$student->id])); ?>" class="btn btn-xs btn-danger">
                                  <span class="glyphicon glyphicon-trash"></span>
                </a>
              </td>
            </tr>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
</div>
  <div class="col-md-6">
  <?php echo $__env->make('admin.includes.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">Assign Students</h3>
      
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <form class="form-horizontal" method="POST" action="<?php echo e(route('student.assign')); ?>">
        <?php echo e(csrf_field()); ?>

          <div class="form-group">
            <label for="season" class="col-sm-2 control-label">Class</label>

            <div class="col-sm-10">
              <select class="form-control" id="season" name="season">
                <option value="<?php echo e($season->id); ?>"><?php echo e($season->title); ?></option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="course" class="col-sm-2 control-label">Course</label>

            <div class="col-sm-10">
              <select class="form-control" id="course" name="course">
                <option value="<?php echo e($course->id); ?>"><?php echo e($course->title); ?></option>
              </select>
            </div>
          </div>
          <div class="form-group">
          <label for="students" class="col-sm-2 control-label">Students</label>
          <div class="col-sm-10">
            <select class="form-control select2" multiple="multiple" id="students[]" name="students[]" data-placeholder="Select student"
                    style="width: 100%;">
                    <?php $__currentLoopData = $season->users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $student): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <option value="<?php echo e($student->id); ?>"><?php echo e($student->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
          </div>
    
        </div>
        <div class="box-footer">
          
          <button type="submit" class="btn btn-success pull-right">Save</button>
        </div>
      </form>
    </div>
    <!-- /.box-body -->
  </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('postscripts'); ?>
  <!-- DataTables -->
  <script src="<?php echo e(asset('bower_components/datatables.net/js/jquery.dataTables.min.js')); ?>"></script>
  <script src="<?php echo e(asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')); ?>"></script>
  <script src="<?php echo e(asset('bower_components/select2/dist/js/select2.full.min.js')); ?>"></script>
  <script>
    $(function () {
      $('.select2').select2()
      // $('#example1').DataTable()
      $('#example1').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
      })
    })
  </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>