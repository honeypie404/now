<?php $__env->startSection('poststyles'); ?>
  <link href="<?php echo e(asset('css/summernote.css')); ?>" rel="stylesheet"></link>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<?php echo $__env->make('admin.includes.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<div class="panel panel-default col-md-6">
    <div class="panel-heading">Edit site settings</div>

    <div class="panel-body">
        <form class="" action="<?php echo e(Route('settings.update')); ?>" method="post" enctype="multipart/form-data">
          <?php echo e(csrf_field()); ?>

          <div class="form-group">
            <label for="site_name">Site name</label>
            <input class="form-control" type="text" name="site_name" value="<?php echo e($settings->site_name); ?>">
          </div>
          <div class="form-group">
            <label for="logo">Logo</label>
            <input class="form-control" type="file" name="logo"><img src="<?php echo e(asset($settings->logo)); ?>" width="60" height="40" alt="">
          </div>
          <div class="form-group">
            <label for="address">Address</label>
            <input class="form-control" type="text" name="address" value="<?php echo e($settings->address); ?>">
          </div>
          <div class="form-group">
            <label for="contact_number">Contact number</label>
            <input class="form-control" type="text" name="contact_number" value="<?php echo e($settings->contact_number); ?>">
          </div>
          <div class="form-group">
            <label for="contact_email">Contact Email</label>
            <input class="form-control" type="email" name="contact_email" value="<?php echo e($settings->contact_email); ?>">
          </div>
          <div class="form-group">
            <label for="about">About</label>
            <textarea class="form-control" id="about" name="about"><?php echo e($settings->about); ?></textarea>
          </div>
          <div class="form-group">
            <button class="btn btn-success" type="submit">
              Update setting
            </button>
          </div>
        </form>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('postscripts'); ?>
  <script type="text/javascript" src="<?php echo e(asset('js/summernote.min.js')); ?>"></script>
  <script>
    $(document).ready(function() {
        $('#about').summernote();
    });
  </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>