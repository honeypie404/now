<?php $__env->startSection('poststyles'); ?>
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo e(asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div class="col-md-6">
  <div class="box box-primary">
    <div class="box-header  with-border">
      <h3 class="box-title"><?php echo e($course->title); ?> Classes</h3>
      
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>Class</th>
          <th>Course</th>
          <th>Students</th>
          <th>Delete</th>
        </tr>
        </thead>
        <tbody>
          <?php $__currentLoopData = $seasons; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $season): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
              <td><?php echo e($season->title); ?></td>
              <td><?php echo e($season->course->title); ?></td>
              <td>
                <a href="<?php echo e(route('class.students',['id'=>$season->id,'course'=>$course->id])); ?>" class="btn btn-xs btn-info">
                                  students</span>
                </a>
              </td>
              <td>
                <a href="<?php echo e(route('course.delete',['id'=>$season->id,'course'=>$course->id])); ?>" class="btn btn-xs btn-danger">
                                  <span class="glyphicon glyphicon-trash"></span>
                </a>
              </td>
            </tr>
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
</div>
  <div class="col-md-6">
  <?php echo $__env->make('admin.includes.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">Create Class</h3>
      
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <form class="form-horizontal" method="POST" action="<?php echo e(route('class.store')); ?>">
        <?php echo e(csrf_field()); ?>

        <div class="form-group">
          <label for="title" class="col-sm-2 control-label">Title</label>

          <div class="col-sm-10">
            <input type="text" class="form-control" id="title" name="title" placeholder="Class Name">
            <input type="hidden" class="form-control" id="course" name="course" value="<?php echo e($course->id); ?>"/>
          </div>
        </div>
        <div class="box-footer">
          
          <button type="submit" class="btn btn-success pull-right">Save</button>
        </div>
      </form>
    </div>
    <!-- /.box-body -->
  </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('postscripts'); ?>
  <!-- DataTables -->
  <script src="<?php echo e(asset('bower_components/datatables.net/js/jquery.dataTables.min.js')); ?>"></script>
  <script src="<?php echo e(asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')); ?>"></script>
  <script>
    $(function () {
      
      // $('#example1').DataTable()
      $('#example1').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
      })
    })
  </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>