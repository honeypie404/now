<?php $__env->startSection('poststyles'); ?>
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo e(asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  

        <div class="col-md-6">
  <?php echo $__env->make('admin.includes.errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">Create Assignment</h3>
     
<div class="box-body">
      <form class="form-horizontal" method="POST" action="<?php echo e(route('assignments.store')); ?>">
        <?php echo e(csrf_field()); ?>

        <div class="form-group">
          <label for="title" class="col-sm-2 control-label">Title</label>

          <div class="col-sm-10">
            <input type="text" class="form-control" id="title" name="title" placeholder="Assignment Title">
          </div>
        </div>
        <div class="form-group">
        <label for="code" class="col-sm-2 control-label">course_id</label>

        <div class="col-sm-10">
          <input type="text" class="form-control" id="code" name="code" placeholder="Course id">
        </div>
      </div>
        <div class="form-group">
          <label for="description" class="col-sm-2 control-label">Description</label>

          <div class="col-sm-10">
            <textarea class="form-control" id="description" name="description" placeholder="Description"></textarea>
          </div>
        </div>
        <div class="form-group">
          <label for="code" class="col-sm-2 control-label">Resourse</label>

          <div class="col-sm-10">
            <input type="text" class="form-control" id="resourse" name="resourse" placeholder="Resourses">
          </div>
        </div>
        <div class="box-footer">
          
          <button type="submit" class="btn btn-success pull-right"> Save</button>
        </div>
      </form>
    </div>
    <!-- /.box-body -->
  </div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('postscripts'); ?>
  <!-- DataTables -->
  <script src="<?php echo e(asset('bower_components/datatables.net/js/jquery.dataTables.min.js')); ?>"></script>
  <script src="<?php echo e(asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')); ?>"></script>
  <script>
    $(function () {
      // $('#example1').DataTable()
      $('#example1').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
      })
    })
  </script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>