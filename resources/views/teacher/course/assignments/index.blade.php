@extends('layouts.frontend')
@section('poststyles')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection
@section('content')
    <div class="col-md-6">
  <div class="box box-primary">
    <div class="box-header  with-border">
      <h3 class="box-title">Assignments</h3>
      {{-- <a href="{{ route('user.create') }}" style="float:right;" class="btn btn-xs btn-success">
                        Create</span>
      </a> --}}
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>Title</th>
          <th>CNo.</th>
          <th>Description</th>
          <th>Edit</th>
          <th>Delete</th>
        </tr>
        </thead>
        <tbody>
          @foreach ($assignments as $assignment)
            <tr>
              <td>{{ $assignment->title }}</td>
              <td>{{ $assignment->cno }}</td>            
              <td>{{ $assignment->description }}</td>
              <td>
              <a href="{{ route('assignments.edit',['id'=>$assignment->id]) }}" class="btn btn-xs btn-info">
                Edit</span>
                </a>
                </td>
              <td>
                <a href="{{ route('assignments.delete',['id'=>$assignment->id]) }}" class="btn btn-xs btn-danger">
                                  <span class="glyphicon glyphicon-trash"></span>
                </a>
              </td>
              
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
</div>
  <div class="col-md-6">
  @include('admin.includes.errors')
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">Create assignment</h3>
      {{-- <a href="{{ route('user.create') }}" style="float:right;" class="btn btn-xs btn-success">
                        Create</span>
      </a> --}}
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <form class="form-horizontal" method="POST" action="{{ route('assignments.store') }}">
        {{ csrf_field() }}
        <div class="form-group">
          <label for="title" class="col-sm-2 control-label">Title</label>

          <div class="col-sm-10">
            <input type="text" class="form-control" id="title" name="title" placeholder="Course Name">
          </div>
        </div>


        <div class="form-group">
          <label for="course" class="col-sm-2 control-label">CNo.</label>
       
          <div class="col-sm-10">
          <select class="form-control select2" multiple="multiple" id="course[]" name="course[]" data-placeholder="Select courses"
                  style="width: 100%;">
                  @foreach ($courses as $course)
                    <option value="{{ $course->id }}">{{ $course->title }} - {{ $course->cno }}</option>
                  @endforeach
          </select>
        </div>
        </div>
        
     
        <div class="form-group">
          <label for="description" class="col-sm-2 control-label">Description</label>

          <div class="col-sm-10">
            <textarea class="form-control" id="description" name="description" placeholder="Description"></textarea>
          </div>
        </div>
        <div class="box-footer">
          {{-- <button type="submit" class="btn btn-default">Cancel</button> --}}
          <button type="submit" class="btn btn-success pull-right">Save</button>
        </div>
      </form>
    </div>
    <!-- /.box-body -->
  </div>
</div>
@endsection
@section('postscripts')
  <!-- DataTables -->
  <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
  <script>
    $(function () {
      // $('#example1').DataTable()
      $('#example1').DataTable({
        'paging'      : true,
        'lengthChange': true,
        'searching'   : true,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
      })
    })
  </script>
@endsection
