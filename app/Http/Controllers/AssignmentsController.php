<?php

namespace App\Http\Controllers;

use App\Assignment;

use App\User;
use App\Season;
use App\Course;
use Illuminate\Support\Facades\Auth;
use Session;
use View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class AssignmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         // get all the assignments
    

        // load the view and pass the nerds
        $courses = Course::all();     
         $assignment = assignment::all();
    
        return View('teacher.course.assignments.index')->with(      ['assignments'=>$assignment,'courses'=>$courses]);
  
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('teacher.course.assignments.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function store(Request $request)
    { 
      

        $assignment = new assignment();
        
        $assignment->cno = $request->cno;
        $assignment->title = $request->title;
        $assignment->description = $request->description;
      
        
        $assignment->save();
        Session::flash('success','assignment information saved.');

        return redirect()->route('assignments.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function show(Assignment $assignment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function edit(Assignment $assignment)
    {
        return View::make('teacher.course.assignments.edit'); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Assignment $assignment)
    {
        $assignment->title = $request->title;
        $assignment->description = $request->description;
  
  
        $assignment->save();
        return redirect()->route('assignments.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Assignment  $assignment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $assignment = assignment::find($id);
   
        $assignment->delete();
        Session::flash('success','assignment information deleted.');
        return redirect()->route('assignments.index');
    }
}
