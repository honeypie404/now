<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
        

    
   public function users()
    {
        return $this->belongsTo('App\User');
    }


    public function course()
    {
        return $this->belongsTo('App\Course');
    }



}
